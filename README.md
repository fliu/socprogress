**Quick Links:**

- [Issue !56](https://salsa.debian.org/freedombox-team/freedombox/-/issues/56)
- [Email server progress thread](https://discuss.freedombox.org/t/email-server-progress/1330)
- [Mailing list](https://alioth-lists.debian.net/pipermail/freedombox-discuss/)

![Tentative Architecture Diagram](Tentative Architecture Diagram.png)

**May 23:** Due to a recent security advisory, we have decided to make Postfix
the default MTA.

**May 30:** I installed a private BIND9 name server in the test environment.

```
sudo apt install bind9
```

Modify `/etc/bind/named.conf.local`

```
zone "test.example" {
    type master;
    file "/etc/bind/db.test.example";
    notify no;
    allow-transfer { none; };
    allow-query { 192.168.0.0/16; };
};
```

Here I added a private zone for `*.test.example`

**WARNING:** Do not use the `.local` TLD! It is reserved for mDNS queries.

Create the zone file `/etc/bind/db.test.example`

```
$ORIGIN test.example.
$TTL 1D

@ IN SOA ns1 noreply (
         2021010199  ; serial
         604800      ; refresh
         86400       ; retry
         2419200     ; expire
         604800 )    ; negative cache TTL

@ IN A 192.168.1.9
@ IN NS ns1

ns1 IN A 192.168.1.10

host1 IN A 192.168.1.20
host1 IN MX 10 mx.host1
mx.host1 IN A 192.168.1.20

host2 IN A 192.168.1.21
host2 IN MX 10 mx.host2
mx.host2 IN A 192.168.1.21
```

For testing purposes, I added an unreachable forwarder in
`/etc/bind/named.conf.options`

```
forwarders { 127.0.0.2; };
```

Check configuration. Check zone. Restart bind9 service.

```
sudo named-checkconf
sudo named-checkzone test.example /etc/bind/db.test.example
sudo systemctl restart bind9
```

Test.

```
dig +short @192.168.1.10 MX host1.test.example
dig +short @192.168.1.10 A mx.host1.test.example
```

**References:**

- [Understanding DNS—anatomy of a BIND zone file | Ars Technica](https://arstechnica.com/gadgets/2020/08/understanding-dns-anatomy-of-a-bind-zone-file/)
- [Advanced DNS Features - BIND 9 documentation](https://bind9.readthedocs.io/en/v9_16_6/advanced.html)
- [Bind9 - Debian Wiki](https://wiki.debian.org/Bind9)

**June 6:** Continue to engage in community discussions. Announced GSoC
deliverables. Familiarized myself with Debian's bug reporting mechanism.

**June 13:** Wrote code to manage Postfix and Dovecot services. Tooling issue
[!2087](https://salsa.debian.org/freedombox-team/freedombox/-/issues/) cannot
get IPv4 lease in container

**June 15:**

Service orientation

- Resources: an alias, a domain, a certificate, a key, a test report, a task
- Operations: GET, POST, PUT, DELETE
- Services: account service, audit service, domain service, queue service
- `PUT /account/alias/{id}` creates a new alias
- `POST /audit/certificate/repair` repairs certificate problems
- Stateless
  - HTTP is stateless
  - Information of previous requests are not available to the current request
    handler
  - Allows horizontal scaling
  - States are managed by an external component (Redis, SQL)
  - Resource addressing (request is being handled by host0-pid1000-thread4)
  - Service migration (edge computing): moves request from host0-pid100-thread4
    to host9-pid200-thread1

Relation to FBX?

- Plinth is a frontend to the email configuration service
- Action script is another frontend
- One resource, many clients
- Must deal with concurrent access

Implementation

- Not going to run a separate server consuming JSON (overkill)
- Use the idea, not the wire protocol
  - GET, POST, PUT, DELETE phraseology
  - No Script Anywhere<sup>TM</sup>: it's just a Python function call
    - `PUT /account/alias/{id}` corresponds to `account.alias.put(id)`
  - Turn functionalities into resources
  - Implement services that manage resources
  - Stateless: caller does not provide information about previous requests
  - Separate data access code from controller logic

Locking and forking

- CherryPy in FBX is single-process
- Imports code in one thread; creates a separate thread for each HTTP request
- Multi-processing can break existing FBX code (address space separation)
- uWSGI uses fork()

A POSIX record lock

- Does not understand threads
- Synchronizes processes, not threads
- Locks inodes, not file objects
- Requires intra-process coordination
- Is not inherited via fork
- [File lock survey](https://gavv.github.io/articles/file-locks/)
- man 2 fcntl

The fork syscall

- Copies mutex states; creates single-threaded child
  - [Do you see how this can cause deadlocks?](https://chengyihe.wordpress.com/2015/10/30/android-child-process-hits-mutex-deadlock-after-fork/)
- Does not preserve file record locks
